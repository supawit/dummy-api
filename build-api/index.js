const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const error = require('./src/error')
const employees = require('./src/employees')

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())

app.options('*', cors())
app.get("/",(req,res)=>{res.status(200).send({"msg":`Hello. ${req.headers.authuser}`})})
app.post("/employees", employees.inputValidate, employees.create)
app.put("/employees/:id", employees.inputValidate, employees.update)
app.get("/employees", employees.getAll)
app.get("/employees/:id", employees.getOne)
app.delete("/employees/:id", employees.delete)

app.use(error.response)

app.listen(8080,'0.0.0.0');
console.log("API is listening on port 8080")