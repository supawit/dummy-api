let error = {}

error.response = (error,req,res,next) => {
    let httpStatusCode = req.httpStatusCode?req.httpStatusCode:500
    
    res.status(httpStatusCode).send(error)
}

module.exports = error