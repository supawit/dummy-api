let employees = {}

const configs = require('./configs')
const MongoClient = require('mongodb').MongoClient
const Ajv = require('ajv')
const ObjectId = require('mongodb').ObjectID

employees.inputValidate = (req,res,next) => {
    let ajv = new Ajv({allErrors:true, removeAdditional:'all' })
    let inputSchema = {
        "title" : "employee input schema",
        "description" : "properties required to create employee",
        "type" : "object",
        "properties" : {
            "firstname" : {
                "type" : "string"
            },
            "lastname" : {
                "type" : "string"
            },
            "birthday" : {
                "type" : "string",
                "format" : "date"
            },           
            "email" : {
                "type" : "string",
                "format" : "email"
            }
        },
        "additionalProperties": false,
        "required": ["firstname", "lastname", "birthday", "email"]
    }

    if (ajv.validate(inputSchema,req.body)) {
        next()
    } else {
        req.httpStatusCode=400
        next(ajv.errors)
    }
}

employees.create = (req,res,next) => {
    MongoClient.connect(configs.mongoConnectionUri,(err,db)=>{
        if (err) return next(err)        
        
        db.db(req.headers.authuser)
        .collection('employees')
        .insertOne(
            req.body,
            (err,result)=>{
                db.close()
                if(err) return next(err)

                res.status(200)
                .send(
                    {
                        result: result.ops
                    }
                )
            }
        )
    })
}

employees.getAll = (req,res,next) => {
    MongoClient.connect(configs.mongoConnectionUri,(err,db)=>{
        if(err) return next(err)

        db.db(req.headers.authuser)
        .collection('employees')
        .find({}).toArray((err,result)=>{
            db.close()
            if(err) return next(err)
            res.status(200)
            .send(result)
        })
    })
}

employees.getOne = (req,res,next) => {
    MongoClient.connect(configs.mongoConnectionUri,(err,db)=>{
        if(err) return next(err)
    
        try {
            let id = new ObjectId(req.params.id)  
            db.db(req.headers.authuser)
            .collection('employees')
            .findOne({_id:id},(err,result)=>{
                db.close()
                if(err) return next(err)
                res.status(200)
                .send(result)
            })  
        } catch (error) {
            next(error)
        }        
    })
}

employees.update = (req,res,next) => {
    MongoClient.connect(configs.mongoConnectionUri,(err,db)=>{
        if(err) return next(err)
        
        try {
            let id = new ObjectId(req.params.id)
            db.db(req.headers.authuser)
            .collection('employees')
            .updateOne({_id:id},{ $set: req.body},(err,result)=>{
                db.close()
                if(err) return next(err)

                res.status(200)
                .send(
                    {
                        result: result
                    }
                )
            })
        } catch (error) {
            next(error)
        }
    })
}

employees.delete = (req,res,next) => {
    MongoClient.connect(configs.mongoConnectionUri,(err,db)=>{
        if(err) return next(err)

        try {
            let id = new ObjectId(req.params.id)
            db.db(req.headers.authuser)
            .collection('employees')
            .deleteOne({_id:id},(err,result)=>{
                db.close()
                if(err) return next(err)

                res.status(200).send(result)
            })
        } catch (error) {
            next(error)
        }
    })
}

module.exports = employees